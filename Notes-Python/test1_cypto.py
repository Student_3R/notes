from cryptography.fernet import Fernet
# Put this somewhere safe!
key = Fernet.generate_key()
f = Fernet(key)
print("the key is " + str(key) + "\n")
token = f.encrypt(b"A really secret message. Not for prying eyes.")
print("the crypto message is "+ str(token) +"\n")
print("the origin message is "+ str(f.decrypt(token)))

